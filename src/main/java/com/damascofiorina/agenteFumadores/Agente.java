package com.damascofiorina.agenteFumadores;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Agente {
    private Semaphore tabaco;
    private Semaphore papel;
    private Semaphore fosforo;
    private Semaphore agente;
    private Semaphore fumador;
    
    private Boolean esPapel;
    private Boolean esFosforo;
    private Boolean esTabaco;
    

    public Agente() {
        
        this.fosforo = new Semaphore(0);
        this.papel = new Semaphore(0);
        this.tabaco = new Semaphore(0);
        this.agente = new Semaphore(0);
        this.fumador = new Semaphore(0);
        
        this.esFosforo = false;
        this.esPapel = false;
        this.esTabaco = false;
        
    }
    
    public void empiezo() {
        
        this.agente.release();
        this.fumador.release();
        int recursoTabaco = 2;
        int recursoFosforo = 0;
        int recursoPapel = 1;
        
        ArrayList<Semaphore> semaforos = new ArrayList();
        semaforos.add(fosforo);
        semaforos.add(papel);
        semaforos.add(tabaco);
        
        ArrayList<Boolean> estanDisponiblesRecursos = new ArrayList();
        estanDisponiblesRecursos.add(esFosforo);
        estanDisponiblesRecursos.add(esPapel);
        estanDisponiblesRecursos.add(esTabaco);

        Fumador fumadorConTabaco = crearFumador("Fumador con tabaco", fosforo, papel, recursoFosforo, recursoPapel, estanDisponiblesRecursos);
        Fumador fumadorConFosforo = crearFumador("Fumador con fosforo", tabaco, papel, recursoTabaco, recursoPapel, estanDisponiblesRecursos);
        Fumador fumadorConPapel = crearFumador("Fumador con papel", tabaco, fosforo, recursoTabaco, recursoFosforo, estanDisponiblesRecursos);

        fumadorConTabaco.start();
        fumadorConFosforo.start();
        fumadorConPapel.start();
        
        while (true) {
            
            this.darRecursosAleatoriamente(semaforos, estanDisponiblesRecursos);
            
        }
        
    }
    
    
    private Fumador crearFumador(String nombre, Semaphore recurso1, Semaphore recurso2, int numeroRecurso1, int numeroRecurso2, ArrayList<Boolean> estanDisponibleRecursos) {
        ArrayList<Semaphore> recursos =  new ArrayList<>();
        recursos.add(recurso1);
        recursos.add(recurso2);
        
        ArrayList<Integer> estanDisponiblesMisRecursos = new ArrayList<>();
        estanDisponiblesMisRecursos.add(numeroRecurso1);
        estanDisponiblesMisRecursos.add(numeroRecurso2);
        
        return new Fumador(nombre, agente, fumador, recursos, estanDisponibleRecursos, estanDisponiblesMisRecursos);
    }
    
    
    private void darRecursosAleatoriamente(ArrayList<Semaphore> semaforos, ArrayList<Boolean> estanDisponiblesRecursos) {
        try {
            this.agente.acquire();
            Random rand = new Random();
            int primero = rand.nextInt(3);
            int segundo = primero;
            while (segundo == primero) {
                segundo = rand.nextInt(3);
            }
            System.out.println("Agente esta activo y libero el recurso: " + primero + " " + segundo );
            semaforos.get(primero).release();
            semaforos.get(segundo).release();
            estanDisponiblesRecursos.set(primero, true);
            estanDisponiblesRecursos.set(segundo, true);

        } catch (InterruptedException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
