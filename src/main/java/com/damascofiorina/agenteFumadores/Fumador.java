package com.damascofiorina.agenteFumadores;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Fumador extends Thread {
    
    private String nombre;
    private Semaphore agente;
    private Semaphore fumador;
    private ArrayList<Semaphore> recursos;
    private ArrayList<Boolean> estanDisponiblesRecursos;
    private ArrayList<Integer> estanDisponiblesMisRecursos;

    public Fumador(String nombre, Semaphore agente, Semaphore fumador, ArrayList<Semaphore> recursos, ArrayList<Boolean> estanDisponiblesRecursos, ArrayList<Integer> estanDisponiblesMisRecursos) {
        this.nombre = nombre;
        this.agente = agente;
        this.fumador = fumador;
        this.recursos = recursos;
        this.estanDisponiblesRecursos = estanDisponiblesRecursos;
        this.estanDisponiblesMisRecursos = estanDisponiblesMisRecursos;
    }
    
    @Override
    public void run() {  
        while (true) {
            try {
                this.fumador.acquire();
                this.fumar();
                this.fumador.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(Fumador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void fumar() {
        Boolean puedoFumar = false;
        for (int i = 0; i < estanDisponiblesMisRecursos.size(); i++) {
            
            if (!this.estanDisponiblesRecursos.get(estanDisponiblesMisRecursos.get(i))) {
                puedoFumar = false;
                break;
            } else {
                puedoFumar = true;
            }
        }
        
        if (puedoFumar) {
            System.out.println(nombre + " puede pedir los recurso");
            try {
                for (int i = 0; i < recursos.size(); i++) {
                    this.recursos.get(i).acquire();
                }
                System.out.println(nombre + " esta fumando");

                
                for (int i = 0; i < estanDisponiblesMisRecursos.size(); i++) {
                    estanDisponiblesRecursos.set(estanDisponiblesMisRecursos.get(i), false);
 
                }
                this.agente.release();

            } catch (InterruptedException ex) {
                for (int i = 0; i < estanDisponiblesMisRecursos.size(); i++) {
                    estanDisponiblesRecursos.set(estanDisponiblesMisRecursos.get(i), false);
 
                }
                this.agente.release();
                System.out.println(ex.toString());
            }
        }
        
    }
}
